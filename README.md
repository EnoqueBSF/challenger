# Imersao20192-desafio

Repositório dos desafios da Imersão 2019.2 - Fábrica de Softwares.

ATENÇÃO - Leia as Regras:

1ª REGRA

Este repositório foi criado com o intuito de guardar os desafios realizados durante a Imersão de 2019.2;


2ª REGRA

Caso o participante suba qualquer arquivo para a branch master ele estará automaticamente DESCLASSIFICADO;


3ª REGRA

Caso o participante suba qualquer arquivo para a Branch cujo não seja a sua (exemplo: meu nome é Carlos Barreto, subirei para Carlos_Barreto) ele estará automaticamente DESCLASSIFICADO;



4ª REGRA

Os desafios devem ser postados até às 14 horas da Sexta-feira (30/08/2019);
