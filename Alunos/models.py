from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuario(AbstractUser):
    cpf = models.CharField(max_length=14, primary_key=True, verbose_name='CPF')
    matricula = models.CharField(max_length=10, unique=True, null=True, verbose_name='Matrícula')
    email = models.EmailField(max_length=100, unique=True, verbose_name='Email')
    administrador = models.BooleanField(default=False, verbose_name='Administrador')
    moderador = models.BooleanField(default=False, verbose_name='Moderador')

    REQUIRED_FIELDS = ('first_name', 'last_name', 'username', 'email', 'cpf', 'administrador', 'moderador')

    USERNAME_FIELD = ('matricula')

    def __str__(self):
        return self.first_name + ' ' + self.last_name