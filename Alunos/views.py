from django.shortcuts import render, redirect, get_object_or_404
from .models import Usuario
from .forms import CadastroForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


User = get_user_model()
def home(request):
    count = User.objects.count()
    return render(request, 'home.html', {
        'count': count
    })

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'usuarioCreate.html', {'form': form})

def usuarioList(request):
    usuarios = Usuario.objects.all()

    return render(request, 'usuarioList.html', {'usuarios': usuarios})

def usuarioCreate(request):
    form = CadastroForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('usuarioList')

    return render(request, 'usuarioCreate.html', {'form': form})

def usuarioUpdate(request, id):
    usuario = get_object_or_404(Usuario, pk=id)
    form = CadastroForm(request.POST or None, instance=usuario)

    if form.is_valid():
        form.save()
        return redirect('usuarioList')

    return render(request, 'usuarioCreate.html', {'form': form})

def usuarioDelete(request, id):
    usuario = get_object_or_404(Usuario, pk=id)

    if request.method == 'POST':
        usuario.delete()
        return redirect('usuarioList')

    return render(request, 'usuarioDelete.html', {'usuario': usuario})