from django.contrib.auth.forms import UserCreationForm, forms
from .models import Usuario

class CadastroForm(UserCreationForm):
    first_name = forms.CharField(
        label='Nome'
    )

    last_name = forms.CharField(
        label='Sobrenome'
    )
    class Meta(UserCreationForm):
        model = Usuario
        fields = ('first_name', 'last_name', 'username', 'matricula', 'cpf', 'email')
        help_texts = {
            'username': None,
        }