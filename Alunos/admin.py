from django.contrib import admin

from .forms import CadastroForm
from .models import Usuario

class UsuarioAdmin(admin.ModelAdmin):
    model = Usuario
    form = CadastroForm

    field = ('first_name', 'last_name', 'username', 'matricula', 'password1', 'password2', 'email', 'cpf', 'administrador', 'moderador')

admin.site.register(Usuario, UsuarioAdmin)