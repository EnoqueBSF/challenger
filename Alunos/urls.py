from django.urls import path, include
from .views import usuarioList, usuarioCreate, usuarioUpdate, usuarioDelete, home, signup


urlpatterns = [
    path('', home, name='home'),
    path('signup/', signup, name='signup'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('list/', usuarioList, name='usuarioList'),
    path('create/', usuarioCreate, name='usuarioCreate'),
    path('update/<int:id>/', usuarioUpdate, name='usuarioUpdate'),
    path('delete/<int:id>/', usuarioDelete, name='usuarioDelete'),
]